"use strict"; // active le mode strict https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Strict_mode

let tab = [
  "la",
  "mère Michelle",
  "a perdu",
  "son chat"
]; // le tableau tab contient des fragmennts de phrase
let sentence = ""; // notre future phrase est une chaine charactère

for (let index = 0; index < tab.length; index++) { // pour chaque éléments du tableau tab
  if (index !== tab.length - 1) { // si je ne suis pas à la fin du tableau
    sentence += `${tab[index]} `; // ajoute dans la phrase le fragment de phrase courant suivi d'un espace
  } else {
    sentence += `${tab[index]}.`; // ajoute dans la phrase le fragment de phrase courant suivi d'un point
  }
}
console.log(sentence) // affiche la phrase
